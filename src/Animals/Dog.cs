using System;

namespace Pets
{
    /// <summary>
    /// Die Klasse implementiert Objekte vom Typ Hund auf der Basis der <c>Pets</c> Klasse.
    /// Er Bellt den Besitzer an
    /// </summary>
    public class Dog : Pet
    {
        public Dog(string name) : base(name) {}

        public override string TalkToOwner() => "Woof!";

        public bool SearchingForCat()
        {
            Random zufall = new Random();  
            if (zufall.Next(0, 10) > 7)
                return true;
            else
                return false;
        }
    }
}
