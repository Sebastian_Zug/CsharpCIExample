# CsharpCIExample

Zeigt die Features von GitLab CI im Kontext eines C#-Projektes. Dabei sollen
insbesondere die automatisierten Test und Buildkonzepte vorgestellt werden.

Das Projekt besteht aus zwei Teilen:
+ eigentlichen Source-Code mit einem Beispielprogramm und 
+ den zugehörigen Tests.
