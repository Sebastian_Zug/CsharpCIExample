using System;

namespace Pets {
    public interface IPet {
        /// <summary>
        /// Property mit dem Namen des Tiers
        /// Die private set Methode muss in den implementierenden Klassen realsiert werden.
        /// </summary>
        string PetName { get; }

        /// <summary>
        /// Das Tier generiert ein Geräusch
        /// </summary>
        /// <returns>
        /// String mit der Imitation des Geräusches
        /// </returns>
        string TalkToOwner ();
    }
}