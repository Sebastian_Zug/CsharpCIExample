using System;
using System.Collections.Generic;
using Pets;

namespace ConsoleApplication {
    public class MyLittleZoo {
        public static void Main (string[] args) {
            List<IPet> pets = new List<IPet> {
                new Dog ("Willy"),
                new Cat ("KatziTatzi")
            };

            foreach (var pet in pets) {
                Console.WriteLine (pet.TalkToOwner ());
            }
        }
    }
}