using System;
using Pets;
using Xunit;

public class PetTests {
    [Fact]
    public void DogTalkToOwnerReturnsWoof () {
        string expected = "Woof!";
        Dog myDog = new Dog ("Willi");
        string actual = myDog.TalkToOwner ();

        Assert.Equal (expected, actual);
    }

    [Fact]
    public void CatTalkToOwnerReturnsMeow () {
        string pattern = "Meow!";
        Cat myCat = new Cat ("Tom");
        string actual = myCat.TalkToOwner ();

        Assert.Contains (pattern, actual);
    }
}