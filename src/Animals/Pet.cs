using System;

namespace Pets {
  public abstract class Pet : IPet {
    public Pet (string name) {
      petName = name;
    }

    string petName;
    public string PetName {
      get { return petName; }
      set { petName = value; }
    }

    public abstract string TalkToOwner ();

    /// <summary>
    /// Das Tier schläft für einige Stunden und sollte nicht gestört werden.
    /// </summary>
    /// <param name="hours">
    /// Anzahl der Stunden
    /// </param>
    void Sleeps (byte hours) {
      Console.WriteLine ($"Iam not avialable for {hours} hours!");
    }
  }
}