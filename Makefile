export DOTNET_SKIP_FIRST_TIME_EXPERIENCE = 1
CSHARP_SOURCE_FILES = $(wildcard */Animals/*.cs */*.cs)

program: $(CSHARP_SOURCE_FILES)
	mcs $(CSHARP_SOURCE_FILES) -out:program

run: program
	mono program

tests:
	dotnet test ./test/AnimalsTests/AnimalsTests.csproj

docu: program
	doxygen Doxyfile
